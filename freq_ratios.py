# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 13:28:23 2022

@author: ramse
"""

import numpy as np
from fractions import Fraction
from math import ceil, sin, pi


#Length of lookup table. try for min of 41, max of 2^8=256
#points per octave, min = 12, must multiply by 2
#overall points = 3*points_per_octave + 4/12 * points_per_octave
#max: 256=3*points_per_octave + 4/12 * points_per_octave
#256/(3+4/12)=ppo = 76
ppo=12*2

#base frequency
base = 4500

val=220*2**(-.25)
n=0
f_want=np.zeros(int(3*ppo + ppo/3)+1)
while True:
    pt=val*2**(n/(ppo))
    f_want[n]=pt
    n=n+1
    if(pt>=1864):
        break

#fout = fbase * (skips+1)/(stay)
#print out desired frequencies
print(f_want)
#print(len(f_want))
f_ratios = f_want/base

f_frac = []
f_table=[]
flag=0
max_den=1
while flag==0:
    for i in range(len(f_ratios)):
        f_frac.append(Fraction(str(f_ratios[i])).limit_denominator(max_den))
        f_table.append([Fraction(str(f_ratios[i])).limit_denominator(max_den).denominator,Fraction(str(f_ratios[i])).limit_denominator(max_den).numerator])
    if len(f_frac) == len(set(f_frac)):
        flag=1
    else:
        max_den=max_den+1
        f_frac=[]
        f_table=[]
#Prints out the ratios in the lookup table
print(f_table)

#Prints out length of lookup table
print(len(f_table))
#print('{')
#for i in f_table:
#    print('{'+str(i[0])+','+str(i[1])+'},')
#print('}')
#print(len(f_table))
#scale_vals1 = [G3 A#3 C4 C#4 D4 F4 G4 A#4 C5]
#scale_vals2 = [F#4 G4 A#4 C5 D#5 F5 F#5 G5 A#5]
#scale_vals3 = [G5 G#5 A5 C6 D6 F6 G6 G#6 A6]
               
#scale_vals1 = [val*2**(1/12), 220*2**(1/12), 220*2**(1/4), 220*2**(1/3), 220*2**(5/12), 220*2**(2/3), 220*2**(5/6), 440*2**(1/12), 440*2**(1/4)]
#scale_vals2 = [220*2**(3/4), 220*2**(5/6), 440*2**(1/12), 440*2**(1/4), 440*2**(1/2), 440*2**(2/3), 440*2**(3/4), 440*2**(5/6), 880*2**(1/12)]
#scale_vals3 = [440*2**(5/6), 440*2**(11/12), 880., 880*2**(1/4), 880*2**(5/12), 880*2**(2/3), 880*2**(5/6), 880*2**(11/12), 1760.]
scale_vals1 = [val*2**(1/12), 220*2**(1/12), 220*2**(1/4), 220*2**(1/3), 220*2**(5/12), 220*2**(2/3)]
scale_vals2 = [220*2**(5/6),  440*2**(1/12), 440*2**(1/4), 440*2**(1/3), 440*2**(5/12), 440*2**(2/3)]
scale_vals3 = [440*2**(5/6),  880*2**(1/12), 880*2**(1/4), 880*2**(1/3), 880*2**(5/12), 880*2**(2/3), 880*2**(5/6),  1760*2**(1/12)]

#Print the frequencies in the first scale for verrification
print(scale_vals1)

#Prints the frequencies and indicies needed for MC1
print('\n\nScale 1')
for i in range(len(scale_vals1)):
    for j in range(len(f_want)):
        if(round(scale_vals1[i])==round(f_want[j])):
            print('F_wanted:'+str(f_want[j])+'\t at index '+str(j))
print('\nScale2')      
for i in range(len(scale_vals2)):
    for j in range(len(f_want)):
        if(round(scale_vals2[i])==round(f_want[j])):
            print('F_wanted:'+str(f_want[j])+'\t at index '+str(j))
print('\nScale3')   
for i in range(len(scale_vals3)):
    for j in range(len(f_want)):
        if(round(scale_vals3[i])==round(f_want[j])):
            print('F_wanted:'+str(f_want[j])+'\t at index '+str(j))



#This prints out the lookup table for MC2, get rid of last comma in the printout         
print('{')
for i in f_table:
    print('{'+str(i[0])+','+str(i[1])+'},')
print('}')